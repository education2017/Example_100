package hr.apps.mosaic.example_100;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

	@BindView(R.id.tvTime) TextView tvTime;
	@BindView(R.id.bStartTime) Button bStartTime;

	private static final String TIME_FORMAT = "hh:mm:ss";
	private static final int UPDATE_TIME = 1000;

	private boolean mIsStarted = false;
	private Handler mHandler = new Handler();
	private Runnable mRunnable = new Runnable() {
		@Override
		public void run() {
			SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());
			String time = format.format(new java.util.Date());
			tvTime.setText(time);
			mHandler.postDelayed(mRunnable, UPDATE_TIME);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
	}

	@OnClick(R.id.bStartTime)
	public void startTime(){
		if(this.mIsStarted){
			this.mHandler.removeCallbacks(this.mRunnable);
			this.bStartTime.setText("Start");
		}else{
			this.mHandler.postDelayed(this.mRunnable, UPDATE_TIME);
			this.bStartTime.setText("Stop");
		}
		this.mIsStarted = !this.mIsStarted;
	}

}
